---
title: "Una Introducción a R para el tratamiento de datos"
subtitle: "<br/> IV Jornadas Doctorales de la UEx"
author: "Javier Vaquero-Martínez"
date: "27 de noviembre de 2020"
output:
  xaringan::moon_reader:
    self_contained: true
    lib_dir: libs
    css: xaringan-themer.css
    nature:
      ratio: '16:9'
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: false
---


```{r xaringan-themer, include = FALSE}
if("xaringanthemer" %in% installed.packages()){
library(xaringanthemer)
  
  primary_color   = "#00AC9A"
  secondary_color = "#f1f4c1"
style_duo_accent(
  primary_color   = primary_color, #"#000080", #"#006747", 
  secondary_color = secondary_color, #"#CFC493",
  ##base_color = "#1c5253",
  code_inline_background_color = secondary_color,
  code_inline_color = primary_color,
  code_highlight_color = secondary_color,
  title_slide_text_color = "#f1f4c1",
  header_font_google = google_font("Montserrat"),
    #google_font("Josefin Sans"),
  text_font_google   = google_font("Montserrat", "300", "300i"),
  ##code_font_google   = google_font("Droid Mono"),
  background_image = "images/fondo_diapos.jpg",
  ##title_slide_text_color = "darkred",
  title_slide_background_image = "images/hextickers_bg.png", #ADD LATER
  text_font_size = "20px",
  extra_css = list(
    "forceBreak"= list(
      "webkit-column-break-after"= "always",
      "break-after"= "column"),
    ".red"   = list( "color"= "red"),
    ".blue"  = list( "color"= "blue"),
    ".small" = list( "font-size"= "30px"),
    ".title" = list( "font-size"= "35px"),
    ".large" = list( "font-size"= "80px")
  )
)
}
```

<style>
.forceBreak { -webkit-column-break-after: always; break-after: column; }
.red { color: red; }
.blue { color: blue; }
.small { font-size: "20px"}
.title{font-size: "25px"}

.col2 {
    columns: 2 200px;         /* number of columns and width in pixels*/
    -webkit-columns: 2 200px; /* chrome, safari */
    -moz-columns: 2 200px;    /* firefox */
}
<!-- .title-slide h1 { -->
<!--   font-size: 35px; -->
<!--   background-color: rgba(10,10,250,0.30); -->
<!--   <!-- opacity: 50% --> -->
<!-- } -->
<!-- .title-slide h2, .title-slide h3 { -->
<!--   font-size: 30px; -->
<!--   <!-- background-color: gray; --> -->
<!--   <!-- opacity: 50% --> -->
<!-- } -->
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo      = FALSE)
knitr::opts_chunk$set(warning   = FALSE)
knitr::opts_chunk$set(prompt    = FALSE)
knitr::opts_chunk$set(comment   = FALSE)
knitr::opts_chunk$set(cache     = TRUE)
knitr::opts_chunk$set(message   = FALSE)
knitr::opts_chunk$set(out.width = '100%')
knitr::opts_chunk$set(comment   = FALSE)
#require(zoo)
require(tidyverse)
#require(lubridate)
#library(rgdal)
library(emo)
#require(ggpubr)
#library(plyr)
#library(dplyr)
#require(ggmap)
#require(mapdata)
#theme_set(theme_grey(base_size=20 ))
```

# Objetivos

- Conocer nociones básicas de __R__.

- Ser capaces de buscar los __paquetes__ `r emo::ji("package")` que nos interesan.

- Hacer análisis, tratamiento de datos y gráficos sencillos.
---
# Índice

1. Motivación.

2. Programación en __R__.

  <!-- + Rstudio -->
  <!-- + Variables y tipos -->
  <!-- + Bucles -->
  <!-- + Condicionales -->
  <!-- + Documentación -->
3. Manejo de datos: __tidyverse__.

  <!-- + Leer datos -->
  <!-- + Transformar datos -->
  <!-- + Analizar datos -->
  <!-- + Gráficas -->
<!-- 4. Algunos casos concretos -->
  <!-- + Estadística -->
  <!-- + Datos espaciales -->
  <!-- + Biología -->
  <!-- + Análisis de corpus (literatura) -->
5. Recursos para aprender más.
---
# 1. Motivación.

**¿Por qué utilizar R?**

- R vs. Excel

   + Reproducibilidad
   
   + Mayor automatización
   
   + Mayor potencia
   
   + Libre y gratuito

???

- Frente a otros lenguajes como Matlab, Python...

   + Hecho para estadísticos para personas que van a usar estadística

---
# Programación en R
## Cómo empezar

1. Descargar e instalar [R](https://cran.rediris.es/) y [RStudio](https://rstudio.com/products/rstudio/download/) /// También podéis usar [RStudio Cloud](https://rstudio.cloud/)

2. Abrir Rstudio.

3. Escribir `2 + 2` en la consola.

4. Escribir `print("¡Hola, mundo!")` en la consola.

---
# Programación en R
## Variables

Las **cosas** que pueden tener un **valor** se llaman variables.

- `x <- 2 + 2`: la variable es `x` y su valor es `4`

- `y <- sqrt(x)`: la variable `y` toma el valor `2`
---
# Programación en R
## Variables
### Tipos de variables

- **Numeric**: números reales.

- **Integer**: números enteros.

- **Factor**: factores.

- **Character**: cadenas de texto (`"¡Hola Mundo!"`)

- **Logical**: Verdadero/Falso (`TRUE` y `FALSE`)

Estos tipos pueden constituir **vectores (1D)**, **matrices (2D)** o **arrays/arreglos (ND)**.

---
# Programación en R
## Variables
### Tipos de variables: Dos tipos especiales

- **Listas**: son como los vectores, pero cada elemento puede ser de un **tipo distinto**. 
<!-- `x[1]` Saca una lista que contiene el primer elemento, `x[[1]]` saca el primer elemento en sí. -->

- **Data.frame**: técnicamente es una lista en la que cada elemento es un vector, con todos los vectores la misma longitud. En la práctica es una **tabla de datos**. Los vamos a usar mucho.

- **Tibble**: los tibble son una especie de **mejora de los data.frame**, pero son prácticamente lo mismo.
---
# Programación en R
## Variables
### Selección de elementos en vectores

.pull-left[
Para vectores
```r
x <- c(1, 9, 6)
x[3] + x[1]
x[2:3]
x[c(TRUE, TRUE, FALSE)]
```
]
.pull-right[
Para matrices:

```r
m <- cbind(c(1,2,3), c(4,5,6), c(7,8,9)) # c de column
m <- rbind(c(1,2,3), c(4,5,6), c(7,8,9)) # r de row

m[2,3]
m[1:3,2]
```
]

---
# Programación en R
## Variables
### Selección de elementos en `data.frame`'s

.pull-left[

Todos seleccionan la **columna** "num_casos", que aparece como un vector.

```r
 escovid$num_casos  #sin comillas, con $
 escovid[["num_casos"]] #estilo lista
 escovid[[14]]
 escovid[,"num_casos"]
```
]
--


.pull-right[

Selecciona las **filas** con número de casos mayores que 10

```r
  escovid[escovid$num_casos>10,]
```
]

---
# Programación en R
## Bucles

```r
a <- 0
for(i in 1:5){

a <- a + x[i]

}
```

Sin embargo, en R rara vez son necesarios los bucles. A menudo las funciones permiten el uso de vectores:

```r
raiz.de.x <- sqrt(x)

a <- sum(x)
```

---
# Programación en R
## Bucles (2)

.pull-left[

Hay toda una familia de funciones para aplicar una función a varios elementos: **funciones apply**.

- apply
- **lapply**
- sapply
- vapply
- mapply
- **tapply**
]

--
.pull-right[
- **`lapply`** aplica la misma función a cada elemento de un vector, y devuelve una lista con todos los resultados. Si usas `sapply`, esta lista se simplifica como se pueda (por ejemplo, convirtiéndola a vector cuando se pueda)

- **`tapply`** aplica la misma función a subconjuntos del vector que compartan la misma característica. Por ejemplo, media de casos de coronavirus por comunidad autónoma:

  `tapply(escovid$casos, escovid$ccaa, mean)`
]


---
# Programación en R
## Condicionales
.pull-left[
- Ejemplo 1:
  ```r
  if(x[1] > 7){
   print("El 1er elemento de x es > 7")
  }
  ```
  ]
  
.pull-right[
- Ejemplo 2

  ```r
  if(x[1] > 7){
    print("El 1er elemento de x es > 7")
  }else{
    print("El 1er elemento de x NO es > 7")
  }
  ```
]

---
# Programación en R
## Documentación

Lo más **importante**. 

- R tiene miles de funciones, no podemos conocerlas todas.
- `?apply # funciones cargadas` 
- `??mutate # funciones en paquetes instalados`

---
# Programación en R
## Instalar y cargar paquetes.

La gran potencia de R es el uso de paquetes: conjuntos de funciones ya definidas que nos simplifican la vida.

- Instalar: `install.packages("nombre-del-paquete")`
- Cargar: `library(nombre-del-paquete)`

- Para los bioinformaticos existe __Bioconductor__

**Nota:** Podemos especificar el paquete del que viene una función con la sintaxis `paquete::funcion()`.

---

# Manejo de datos: tidyverse

- El tidyverse es un conjunto de paquetes que tienen una sintaxis coherente entre ellos.

- Para instalarlo `install.packages("tidyverse")`, y la primera vez que lo vayas a usar cada vez que abras Rstudio `library(tidyverse)`.
---
## Manejo de datos: tidyverse

```{r tidyverse, out.width='60%', fig.align='center'}
knitr::include_graphics("images/tidyverse.png")
```

---
# Manejo de datos: tidyverse
## Leer datos


- Ficheros de texto (txt, dat, csv): `read.table()` y `read.csv`

- Ficheros Excel: paquete `readxl` -> `read_excel()`

- Formato propio de R (RDS): `readRDS`

- Se puede escribir cambiando `read` por `write`

---
# Manejo de datos: tidyverse
## Transformar datos
- Se hace un uso intensivo de la _pipe_ `%>%`. Esto permite introducir lo que hay a la izquierda en la función de la derecho. Ejemplo: `c(1,2,3,4) %>% mean()` es lo mismo que `mean(c(1,2,3,4))`. Esto mejora la lectura:

  `crea_grafico(calcula_medias(misdatos))`

  `misdatos %>% calcula_medias() %>% crea_grafico()`
---
# Manejo de datos: tidyverse
## Transformar datos: trabajo con tabla

- `select()` para quedarnos con una selección de columnas.

- `filter()` para quedarnos con una selección de filas.

- `arrange()` para ordenar los datos en función de una columna.

- `mutate()`, `mutate_if()` para crear nuevas columnas.

- `pivot_longer()` y `pivot_wider` para reordenar la tabla.

---
# Manejo de datos: tidyverse
## Transformar datos: resumiendo

- `group_by()` para agrupar los datos.

- `summarise()`, `summarise_if()` para resumir los datos.

---
# Manejo de datos: tidyverse
## Transformar datos: combinando

- `bind_cols()` une columnas y `bind_rows()` une filas.

- `left_join()`, `right_join()`, `inner_join()`, `full_join` une por las columnas comunes (o las que nosotros especifiquemos). Argumentos `by` y `suffix`.

---

# Manejo de datos: tidyverse
## Analizar

Esto es más propio de R base

- Tenemos funciones para calcular estadísticos (`mean()`, `sd()`, `IQR()`, `median`,...) y regresiones lineales con `lm()`

- Interesante la función `summary()` para obtener información estadística de los datos o modelos.

---

# Manejo de datos: tidyverse
## Gráficos

Paquete `ggplot2`.

- Filosofía: existen elementos que vamos a asignar a variables. A esto le llamaremos _aesthetics_ o _aes_ para los amigos.

- El gráfico se compone de elementos geométricos, _geoms_ que vamos añadiendo como si fueran capas en una transparencia.

- Muchos **ejemplos de gráficos** en https://edav.info/index.html
---
# Manejo de datos: tidyverse
## Gráficos

- `qplot(x,y, data,...)` es una función para hacer un gráfico rápido. El tipo de grafico por defecto lo da el tipo de datos que estemos usando como x e y.

- `ggplot()` crea el gráfico en sí, sin nada.

- `geom_point()` para poner datos en forma de puntos.

- `geom_line()` para poner datos en forma de líneas.

- `geom_bar()`, `geom_col()` gráficos de barras.

- `geom_abline()` para pintar una recta (dando pendiente y ordenada)

- `geom_boxplot()` para pintar un gráfico de cajas


---
# Manejo de datos: tidyverse
## Gráficos: modificando el aspecto

En ggplot existen _themes_, temas. Cada una tendrá un aspecto. Incluso paquetes de R con más temas.

- En ggplot2: `theme_bw()`, `theme_void()`, `theme_dark()`,...

- En ggpubr: `theme_pubclean()`, `theme_pubr()`,...

- En ggthemes: `theme_excel()`, `theme_economist()`, `theme_Stata()`,...

---
# Manejo de datos: tidyverse
## Gráficos: modificando el aspecto

También puedes modificar aspectos concretos con la función `theme()`.

- Ejemplo: 

  ```r
   ggplot(data=df, aes(x=x,y=y))+
     geom_point()+
     theme_bw()+
     theme(legend.position="top")
  ```
  
---
# Manejo de datos: tidyverse
## Gráficos: facets

Las facets es una manera de hacer el mismo gráfico para comparar varios niveles de alguna variable (hombres/mujeres, rangos de edad, países, etc.)

- `facet_grid(factorFila~factorColumna)` hará una matriz de gráficos con los valores de factorFila para las filas y con los valores de factorColumna para las columnas. Si solo queremos una de las dos se deja vacío.

- `facet_wrap(~factor, nrow=, ncol=)` hará una matriz de gráficos con los valores de factor repartidos en `nrow` filas y `ncol` columnas.

---
# Manejo de datos: tidyverse
## Bonus

Algunos paquetes que merece la pena mirar son:

- `r emo::ji("package")` Stringr para manejar texto.
- `r emo::ji("package")` Forcats para trabajar con factores
- `r emo::ji("package")` knitr y `r emo::ji("package")` rmarkdown permiten crear documentos (libros, webs, artículos, en distintos formatos) con la sintaxis de LaTeX o Markdown.

- Si trabajáis con modelos, `r emo::ji("package")` broom os puede ayudar.

- `r emo::ji("package")` ggplotAssist o `r emo::ji("package")` esquisse os puede ayudar.

- `r emo::ji("package")` plotly para gráficos interactivos.

---
# Recursos

- Chuletas: [https://rstudio.com/resources/cheatsheets/](https://rstudio.com/resources/cheatsheets/). Os he dejado algunas (en castellano) en el campus virtual.

- Documentación de R.

- R para la ciencia de datos: [https://es.r4ds.hadley.nz/](https://es.r4ds.hadley.nz/). Libro online muy interesante.
---
layout: false
class: inverse, middle, center
background-image:url("images/hextickers_bg.png")


.center[
**.**

**.**

**.**

.large[**¡Muchas gracias!**]

.large[˚**¿Alguna pregunta o comentario?**]

**.**

**.**

**.**
]